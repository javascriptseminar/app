angular.module('quizNodeApp.modules').controller('HighscoreController', [ '$scope', 'SocketService', 'QuizService', function($scope, SocketService, QuizService) {

	console.log("HighscoreController loaded");

    //init the highscore emit 
    QuizService.triggerHighscoreEmit(function (data) {
        if (data == 'ok') {
            console.log("Triggering higscore ok");
        } else {
            console.log("Higscore sending failed: " + data);
        }
    });

	SocketService.on('highscore', function(data) {
        console.log("received data: " + data);

        //render the higscore list each time the data is updated
        $scope.highscore = JSON.parse(data);

    });

    $scope.$on('$destroy', function (event) {
        SocketService.removeAllListeners();
    });


}]);