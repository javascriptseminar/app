// modules =================================================
var express        = require('express');
var mongoose       = require('mongoose');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var app 		   = require('express')();
var server 		   = require('http').Server(app);
var io 			   = require('socket.io')(server);
var customlogger   = require('./app/customlogger');

// configuration ===========================================
	
// config files
var db = require('./config/db');

var port = process.env.OPENSHIFT_NODEJS_PORT  || 8080; 
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

mongoose.connect(db.url); 


//parse json content in the body
app.use(bodyParser.json()); // parse application/json 
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded

//this is needed in order to simulate DELETE/PUT method over forms
app.use(methodOverride('X-HTTP-Method-Override'));


//used for serving static files
app.use(express.static(__dirname + '/public')); 


//custom filter
app.use(customlogger.logger);


require('./app/routes')(app, io); // pass our application into our routes

// start app ===============================================
server.listen(port, ip);
console.log('Magic happens on ' + ip + ":" + port);


exports = module.exports = app;