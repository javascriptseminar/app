# Node.js Quiz Application for Javascript Seminar

This is a sample app which should be demonstrate some features of node.js

## Installation
1. Download the repository
2. Install npm modules: `npm install`
3. Install bower dependencies `bower install`
4. Start up the server: `node server.js`
5. View in browser at http://localhost:8080 (Do not use https!)

Copyright 2015 Dominik Oppmann & Michael Remmler