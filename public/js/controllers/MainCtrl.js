angular.module('quizNodeApp.modules').controller('MainController', ['$scope', '$location', function($scope, $location) {

	//$scope.tagline = 'NodeQuiz';	
	$scope.subtitle = "Welcome to our Node.js Quiz App!"

    $scope.start = function() {
        $location.path("/game/start");
    }

}]);