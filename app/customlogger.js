var fs 				= require('fs');
var outputStream 	= fs.createWriteStream('./public/assets/log.txt');
var zlib			= require('zlib');


module.exports = {
	
	//simple method to log request to file
	logger : function (req, res, next) {

		var now = new Date();
		var entry = now.toString() + "\t" + req.protocol + "\t" + req.ip + "\t" + req.path + "\n";
		outputStream.write(entry);

		/* 

			This next call is needed to pass the request to the next middleware layer. 
			If that isn't the case, the processing of the request will stop here...
		*/
		next();

	},
	zip : function(response) {
	
		/*
			Advantage of using a stream here:
				If no stream is used (fs.readFile), the complete file must be read in, the
				compression is done and the file is sent as result

				But this approach has some serious disadvantages. If the file is too large, 
				first the complete file must be read into a buffer, which can cause a high 
				memory consumption for many requests. 

				The streaming approach write the data in chunks to the response stream, so 
				the data can be faster served and the memory consumption is low. 

				Further information can be found in the node streaming handbook 
				https://github.com/substack/stream-handbook

		*/
		var inputStream = fs.createReadStream('./public/assets/log.txt');
		inputStream.pipe(zlib.createGzip()).pipe(response);
	}

}