
var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

// create a schema
var questionSchema = new Schema({
    text: { type: String, required: true, unique: true},
    answerA: { type: String, required: true},
    answerB: { type: String, required: true},
    answerC: { type: String, required: true},
    answerD: { type: String, required: true},
    correctAnswer: { type: String, required: true},
    created_at: Date
});


//schema must be exported in order to be available outside this file
module.exports = mongoose.model('Question', questionSchema);