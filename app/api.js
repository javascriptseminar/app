var Question 	= require('./models/Question');
var fs 			= require('fs');

var highscore = {};

/*
	The following functions can be considered as "private" functions, 
	because they aren't exported
*/

function readDataFromFile(callback) {
	fs.readFile('./public/assets/questions.json', function (err, data) {
		if (!err) {
			questions = JSON.parse(data);
			insertDataInDB(questions, callback);
		} else {
			console.log("Could not read data: " + err.message);
			callback(err);
			return;
		}
	});
}

function insertDataInDB(questions, callback) {

	Question.collection.insert(questions, function (err, docs) {
	    if (!err) {
	       	console.info('%d questions were successfully stored.', docs.length);
	       	//we're done, call callback with no error
	       	callback(undefined);
	    } else {
	    	callback(err);
	    	return;
	    }
	});

}

function resetDB(callback) {
	Question.remove({}, function(err) { 
		console.log('collection removed');
		
		// if there is an error, immediatly stop processing and return error
		if (!err) {
			readDataFromFile(callback);
		} else {
			callback(err);
			return;
		}		
	});
}

//only a wrapper for starting the whole import process
// resetDB --> readDataFromFile --> insertDataInDB
function importData(callback) {
	resetDB(callback);
}

function compare(a,b) {
  if (a.points > b.points)
    return -1;
  if (a.points < b.points)
    return 1;
  return 0;
}

function createHighscoreJSON () {
	//highscore is used as hashmap
	//now loop through this hashmap and build the result
	var result = [];

	for (entry in highscore) {
		
		var highscoreEntry = {
			username: 	entry,
			points: 	highscore[entry]
		};

		result.push(highscoreEntry);

	}

	result.sort(compare);

	return result;
}

// this will be exported
module.exports = {

	//demo function for greeting 
	hello : function () {
		console.log ("hello world from api");
		return;
	},
	

	/*
		
		The following code snippet referrs to the "callback hell". As you can see, 
		the code is nested into many levels and it's hard to understand what it does. 
		This example is quite simple, because it only clears a database, reads the 
		questions from a file and insert them into the database. 

		Question.remove({}, function(err) { 
			if (!err) {
				fs.readFile('./public/assets/questions.json', function (err, data) {
					if (!err) {
						Question.collection.insert(JSON.parse(data), function (err, docs) {
						    if (!err) {
								//we are done here...
						    } else {
								console.log(err);
								return;
						    }
						});
					} else {
						console.log(err);
						return;
					}
				});
			} else {
				console.log(err);
				return;
			}		
		});


	*/

	createSampleData : function(callback) {
		importData(callback);
		return;
	},

	findQuestions : function(callback) {
		Question.find({}).exec(function(err, question) {
			if (!err) {
				callback(null, question);
			} else {
				console.log(err);
				callback(err);
			}
		});
		return;
	},

	//updates the live statistics
	statisticUpdate : function(data, callback) {
		

		// the json looks like this
		/*
		
		 	[ {user: domi, points: 5, place: 1}, {user: michael, points: 7, place: 1}, .. ]
		 
		*/

		console.log("In statisticUpdate: " + JSON.stringify(data));

		var username = data['username'];
		var userHigh = highscore[username];


		console.log("Username: " + username);
		console.log("Points: " + userHigh);

		if (userHigh == undefined) {
			highscore[username] = 1;
		} else {
			//user exists
			highscore[username] = ++userHigh;
		}

		return callback (null, createHighscoreJSON());
	}, 

	getHighscore: function() {
		return createHighscoreJSON();
	},

	clearHighscore: function() {
		highscore = {};
		return;
	}


};