angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider
		
		.when('/game/start', {
			templateUrl: 'views/gameprepare.template.html',
			controller: 'QuizPrepareController'
		})

		.when('/game/:user/question/:id', {
			templateUrl: 'views/game.template.html',
			controller: 'QuizController'
		})

		.when('/highscore', {
			templateUrl: 'views/highscore.template.html',
			controller: 'HighscoreController'
		})

		// home page
		.when('/', {
			templateUrl: 'views/home.template.html',
			controller: 'MainController'
		});


	//this is used to remove the # in the url
	$locationProvider.html5Mode(true);

}]);