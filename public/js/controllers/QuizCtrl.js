angular.module('quizNodeApp.modules').controller('QuizController', ['$scope', 'QuizService', '$routeParams', '$location', '$timeout', function($scope, QuizService, $routeParams, $location, $timeout) {

	var id = $routeParams.id
	var username = $routeParams.user;
	console.log("Quiz Controller loaded for Question " + id);

	$scope.showQuestions = true;

	QuizService.getQuestionById(id, function(err, question) {

	    if(err) {
	    	//check the error code
	    	if (err.code == "100") {
	    		//no more questions available, show end page
	    		console.log("end of quiz");
	    		$scope.showQuestions = false;
	    		$scope.username = username;
	    		
	    		QuizService.getMyStats(function(data) {
	    			$scope.myPoints = data;
	    		});
	    		

	    		QuizService.getTotalNumberOfQuestions(function(data) {
	    			$scope.totalQuestions = data;
	    		});
	    	}

	    	return;
	    }

	    //no errors, load the question
	    $scope.question = question.text;
		$scope.answerA = question.answerA;
		$scope.answerB = question.answerB;
		$scope.answerC = question.answerC;
		$scope.answerD = question.answerD;
		$scope.correctAnswer = question.correctAnswer;


	});


	$scope.selectAnswer = function (data) {
		console.log(data + " clicked");

		//call evalute from service...
		QuizService.evaluateAnswer(id, data, function () {
			//answer is correct, highlight this button green
			$("#btn" + data).removeClass("btn-default").addClass("btn-success");
			QuizService.updateStatistics(username); // update statistics in realtime +1
		}, function() {
			//answer is wrong, highlight this button red and the correct one green
			$("#btn" + data).removeClass("btn-default").addClass("btn-danger"); // selected button red
			$("#btn" + $scope.correctAnswer).removeClass("btn-default").addClass("btn-success"); // selected button red
		});

		//neverless if question has been answered correctly or not, forward to next question
		$timeout(function(){ 
			newId = ++id;
			$location.path("/game/" + username + "/question/" + newId);
		},1500);
	}


	$scope.restart = function() {
		$location.path("/game/start");
	}

	$scope.highscore = function() {
		$location.path("/highscore");
	}

}]);


