module.exports = function(app, io) {

	var api         	= require('./api');
	var customlogger 	= require('./customlogger');
	
	api.hello();

	//initialize sample data
	app.get('/samplequiz', function (req, res) {
		api.createSampleData(function(err) {
			if (!err) {
				res.send('ok');
			} else {
				res.send(err);
			}
		});
	});

	app.get('/reset', function(req, res) {
		api.clearHighscore();
		res.send('ok');
	});

	app.get('/log.gz', function(req, res) {
		customlogger.zip(res);
	});

	//return the questions
	app.get('/api/v1/question', function(req, res) {
		api.findQuestions(function(err, data) {
			if (!err) {
				res.send(data);
			} else {
				res.send(err);
			}
		});
	});

	//triggers the emit of highscore
	app.get('/api/v1/emitStats', function(req, res) {
		io.emit('highscore', JSON.stringify(api.getHighscore()));
		res.send('ok'); // only used to end http request, the data is retrieved over websockets
	});


	/*
		This is the default route, which sends the index.html file
		to the browser. This file contains references to all needed 
		javascript files. So no other routes are needed, that sends
		any html code. 
	
	*/
	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});

	


	/*
		The following code handles the connections for websockets. 
		At first node.js listens to a connetion-event. If then a 
		websocket client emits the statUpdate-event, the specific
		method of the api is called

		If a client disconnects, the disconnect-event is triggered. 



	*/

	io.on('connection', function(socket) {
	    console.log('Client connected.');
	   
	    socket.on('statUpdate', function(data){
	    	api.statisticUpdate(data, function (err, data) {
	    		if (!err) {
	    			console.log("Emit highscore: " + JSON.stringify(data));
	    			io.emit('highscore', JSON.stringify(data));
	    		}
	    	});

	    });

	    socket.on('disconnect', function() {
	        console.log('Client disconnected.');
	    });
	});

};