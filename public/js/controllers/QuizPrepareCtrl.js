angular.module('quizNodeApp.modules').controller('QuizPrepareController', ['$scope', 'QuizService', '$location',  function($scope, QuizService, $location) {

	$scope.startGame = function() {
		console.log("Username: " + $scope.username);

		//load the questions 
		QuizService.getQuestions(function(data) {
			//it can be ensured, that the questions are loaded
			//foward to first question
			$location.path("/game/" + $scope.username + "/question/0");
		});

	}


}]);


