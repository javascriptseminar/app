angular.module('quizNodeApp.modules', []);
angular.module('quizNodeApp.services', []);
angular.module('quizNodeApp', ['ngRoute', 'appRoutes', 'quizNodeApp.modules', 'quizNodeApp.services']);