angular.module('quizNodeApp.services').factory('QuizService', ['$http', 'SocketService',  function($http, SocketService) {

    //error codes
    var NO_QUESTION_AVAILABE = "100";

 	var factory = {
        questionList: [],
        numberOfQuestions: 0,
        localStats: 0
    };

	console.log("Quiz Service loaded");
    
    //is it better to receive the data in the service?
    /*
    SocketService.on('highscore', function(data) {
        console.log("FROM QUIZSERVICE: " + data);
    });
    */
    
    /*
     * Nework functions 
     */

    factory.getQuestions = function(callback) {
    	console.log("getQuestions invoked");

        //reset the local statistics if questions are loaded
        factory.localStats = 0;

 		$http.get('/api/v1/question')
            .success(function(data) {
                factory.questionList = data;
                factory.numberOfQuestions = data.length;
                callback(data); // call the callback
            })
            .error(function(data) {
                console.log('Error from Service: ' + data);
        });
    }


    factory.triggerHighscoreEmit = function(callback) {
        $http.get('/api/v1/emitStats')
            .success(function(data) {
                callback(data);
            })
            .error(function(data) {
                console.log('Error from Service: ' + data);
        });
    }


    factory.updateStatistics = function(username) {
         console.log("Update stats from service");

        factory.localStats++;

         data = {
            username: username,
            value: 1
         };

         SocketService.emit('statUpdate', data , function(err, res) {
            console.log(err);
            console.log(res);
         });
    }


    /*
     * General business logic
     */

    factory.getQuestionById = function (id, callback) {

        if (id < factory.numberOfQuestions) {
            question = factory.questionList[id]; //load the question
            callback(null, question); //call the callback fucntion with the question
        } else {
            error = new Error("No more question available");
            error.code = NO_QUESTION_AVAILABE;
            callback(error, null);
        }

    }

    //TODO: Implement this on server side to prevent fraud
    factory.evaluateAnswer = function (id, answer, onSuccess, onFailure) {

        question = factory.questionList[id];
        if (question.correctAnswer == answer) {
            onSuccess();
        } else {
            onFailure();
        }

    }

    factory.getMyStats = function (callback) {
        callback(factory.localStats);
    }

    factory.getTotalNumberOfQuestions = function(callback) {
        callback(factory.numberOfQuestions);
    }

    return factory;
	

}]);