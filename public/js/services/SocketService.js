angular.module('quizNodeApp.services').factory('SocketService', function($rootScope) {
        
        //Production ATTENTION: USE HTTP INSTEAD OF HTTPS
        //var socket = io.connect('http://nq-muc2015.rhcloud.com:8000', {reconnect: true});
        
        //Testing
        var socket = io.connect('http://localhost:8080', {reconnect: true});
        return {
            on: function(eventName, callback) {
                socket.on(eventName, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function(eventName, data, callback) {
                if(typeof data == 'function') {
                    callback = data;
                    data = {};
                }
                socket.emit(eventName, data, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        if(callback) {
                            callback.apply(socket, args);
                        }
                    });
                });
            },
            emitAndListen: function(eventName, data, callback) {
                this.emit(eventName, data, callback);
                this.on(eventName, callback);
            },
            removeAllListeners: function() {
                socket.removeAllListeners();
            }
        };
    });